var finesse = finesse || {};
finesse.gadget = finesse.gadget || {};
finesse.container = finesse.container || {};
if (!window.console) window.console = {}; 
if (!window.console.log) window.console.log = function () { }; 

/** @namespace */
// Configuration for the gadget
finesse.gadget.Config = (function () {
	var _prefs = new gadgets.Prefs();

	/** @scope finesse.gadget.Config */
	return {
		authorization: _prefs.getString("authorization"),
		host: _prefs.getString("host"),
		restHost: "localhost"
	};
}());

/** @namespace */
finesse.modules = finesse.modules || {};
finesse.modules.SampleGadget = (function ($) {
    var numDialogs = 0;	     // used to count the calls (dialogs)
	var callvars = new Array();  // the callvars array of callvariables		
    var user, states, dialogs,dialogObject,dialogStatus,

    makeCallSuccess = function(rsp) { },
    
    makeCallError = function(rsp) { },


    handleConsultCall=function(event)
    {
        console.error(event.data);
        dialogObject.makeConsultCall(
            46717,
            46714,
            {
                success: makeCallSuccess,
                error: makeCallError
}
            );
    },

    render = function (dialog)
    {
        dialogObject=dialog;
        var currentState = user.getState();
		if (dialog.getState()==dialogStatus.ACTIVE)
        {
            var o=document.getElementById("content_daleel");
            console.error(o.contentWindow);
            o.contentWindow.postMessage(dialog.getFromAddress(),"http://192.168.200.164:8080/Daleel/index2.html");
            window.addEventListener('message',handleConsultCall,false);
    		gadgets.window.adjustHeight();

		}
        else
        {   
            var o=document.getElementById("content_daleel");
            console.error(o.contentWindow);
            o.contentWindow.postMessage("  ","http://192.168.200.164:8080/Daleel/index2.html");
	    }   
    },
    handleNewDialog = function(dialog)
    {
        console.log("In handleNewDialog screenpop");
	    numDialogs++;
        callvars = dialog.getMediaProperties();
        var s= dialog.getMediaProperties();
        dialog.addHandler('change',render);
    },

    handleEndDialog = function(dialog) {
    
	   // decrement the number of dialogs
            numDialogs--;
 
            // render the html in the gadget
            render(dialog);
    },
    
    handleUserLoad = function (userevent)
    {
        dialogs = user.getDialogs( {
            onCollectionAdd : handleNewDialog,
            onCollectionDelete : handleEndDialog
        });
         
        render();
    },

    handleUserChange = function(userevent) {
      
    };
	    
	/** @scope finesse.modules.SampleGadget */
	return {

	    init : function () {
			var prefs =  new gadgets.Prefs(),
			id = prefs.getString("id");
	        gadgets.window.adjustHeight();
	        finesse.clientservices.ClientServices.init(finesse.gadget.Config);
	        user = new finesse.restservices.User({
				id: id, 
                onLoad : handleUserLoad,
                onChange : handleUserChange
            });
	            
	        states = finesse.restservices.User.States;
            dialogStatus = finesse.restservices.Dialog.States;
	    }
    };
}(jQuery));
