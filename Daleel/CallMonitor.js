/**
* Auteur Olivier Jacquemin
* revis� par Salem Ben Afia 
* date derni�re r�vision 18-02-2013
* 
* Version  : v2
* Integration ACP-Daleel
*
**/

function CallMonitor() {

	var self = this;
	
    	/**
	* phone number exposed
	**/
	this.phoneNumber = "";
    /**
     * Identifier of this client application.
     * 0 for requesting a new one by GetUserData().
     */
    var clientAppId = 0;
	var getEventIntervall=null; 
	/**
	*num�ro de l'appelant
	*/
	this.clid = ""
	/**
	* Indique la possibilit� du ConsultationCall
	* C1 - Dernier VoiceCallState= connected
	* C2 - MER Possible????
	* C3 - VoiceCall.service <> ""
	**/
/**/this.canTransfer = false;
	/**
	* last Voice Call
	**/
/**/var lastVoiceCall = null;
	/**
	* last Voice call state
	**/
/**/var lastVoiceCallState = null;
	/**
	* variableValuesToBlockExtTransf
	**/
/**/var blockTransferValue ="";
    /**
     * Whether an invocation of GetEvent() is pending or not.
     */
    var invokingGetEvent = false;
	
    /**
     * Identifier of the current call, or empty string if none.
     */
    var currentCallId = "";

    /**
     * Utility method to create call object needed to invoke web service method.
     */
    function buildCallObject(method, timeout) {
	    var callObj = App.adapter.createCallOptions();
	    callObj.async = true;
	    callObj.timeout = timeout * 1000;
	    callObj.funcName = method;
	    callObj.SOAPHeader = [ {} ];

        return callObj;
    }

    /**
     * ACP >= V21
     * Futuriste
     */
    function callbackGetInitialUserData(result) {
        if (result.error) {
           // console.error("CallMonitor: GetInitialUserData(): " + result.errorDetail.string);
            return;
        }
        
        //console.log("CallMonitor: GetInitialUserData() returned successfully.");

        clientAppId = result.value.ClientApplicationId;
        //console.info("Identifier of client application: " + clientAppId);

        setUserPhone(agentPhone);
    }

    /**
     * ACP <= V12
     */
    function callbackGetUserDataEx02(result) {
        if (result.error) {
           // console.error("CallMonitor: GetUserDataEx02(): " + result.errorDetail.string);
            return;
        }
        
       // console.log("CallMonitor: GetUserDataEx02() returned successfully.");
		
        clientAppId = result.value.clientApplicationId;
		blockTransferValue = result.value.variableValuesToBlockExtTransf;
		blockTransferValue = blockTransferValue.replace(/^\s+|\s+$/g, '');
		//console.info("blockTransferValue : " + blockTransferValue);
		var phoneNumber = result.value.phoneNumbers[0];
		//console.info("Identifier of client application: " + clientAppId);
		if(phoneNumber && phoneNumber != "0"){
			setUserPhone(phoneNumber);
		}else
		{
			alert("Veuillez s�lectionner votre t�l�phone avant de lancer Dalil");
		}
    }


   this.init = function() {
//        console.log("CallMonitor: invoking GetInitialUserData().");
        //console.log("CallMonitor: invoking GetUserDataEx02().");
        var requiredEvents = [
            "VoiceCallState",
            "VoiceCall",
            "CallAnyMediaCancelled",
            "AreYouAlive",
            "SessionClosed"
        ];
    	var callObj = buildCallObject("GetUserDataEx02", 0);
//    	var callObj = buildCallObject("GetInitialUserData", 0);
//	    App.adapter.callService(callbackGetInitialUserData,
	    App.adapter.callService(callbackGetUserDataEx02,
                                callObj,
                                requiredEvents,
                                clientAppId);
    };

    function callbackSetUserPhone(result) {
        if (result.error) {
            //console.error("CallMonitor: SetUserPhone(): " + result.errorDetail.string);
            return;
        }
        //console.log("CallMonitor: SetUserPhone() returned successfully.");

        startGetEvent();
    }

    function setUserPhone(number) {
        //console.log("CallMonitor: invoking GetEvent().");
	    App.adapter.callService(callbackSetUserPhone,
                                "SetUserPhoneEx01",
                                clientAppId,
                                number);
    }

    function callbackGetEvent(result) {
        invokingGetEvent = false;

        if (result.error) {
            //console.error("CallMonitor: GetEvent(): " + result.errorDetail.string);
            return;
        }

        var clientEvents = result.value;
        var clientEvent;
        while (clientEvents.length > 0) {
            clientEvent = clientEvents.shift();
            //console.log("CallMonitor: GetEvent() returned " + clientEvent.EventType);
            switch(clientEvent.EventType) {
            case "VoiceCallState":
				lastVoiceCallState = clientEvent;

				if(lastVoiceCallState.CurrentState == "Idle")//clear clid if idle state
				{	
					lastVoiceCallState = null;
					lastVoiceCall = null;
					self.clid = "";
					//appel vers l'interface flex via JS
					window.callReceived(self.clid);
				}
				else
				{
					self.clid = lastVoiceCallState.FirstHalfCall.Clid;
					//appel vers l'interface flex via JS
					window.callReceived(self.clid);
				}
				break;
            case "VoiceCall":
				lastVoiceCall = clientEvent;
				currentCallId=clientEvent.ID;
				
				
                		break;
            case "CallAnyMediaCancelled":
               			 break;
            case "SessionClosed":
				
                		break;
            default: // AreYouAlive
                		break;
            }
        }
		checkCanTransfer();
		//console.log("this.canTransfer : " + self.canTransfer);
		
		
    }
	
    function getEvent() {
        if (invokingGetEvent) return;
        invokingGetEvent = true;
		
        //console.log("CallMonitor: invoking GetEvent().");
    	var callObj = buildCallObject("GetEvent01", 0);
	    App.adapter.callService(callbackGetEvent,
                                callObj,
                                clientAppId,
                                false);
    }

    function callbackConsultationCall(result) {
        
	if (result.error) {
            console.error("CallMonitor: ConsultationCall(): " + result.errorDetail.string);
        }
        else {
            console.log("CallMonitor: ConsultationCall() returned successfully.");
        }
	
    }

    this.consultationCall = function(destination) {            
   		if (this.canTransfer) {
            console.log("CallMonitor: invoking ConsultationCall(" + destination + ").");
            App.adapter.callService(callbackConsultationCall,
                                    "ConsultationCall",
                                    currentCallId,
                                    destination);
	    
        } else {
            console.warn("Ignoring CallMonitor.consultationCall(): no current call.");
        }
    };

    function startGetEvent() {
		
        console.log("CallMonitor: starting GetEvent() invocations.");
        
		getEventIntervall=window.setInterval(getEvent, 500);//continue � invoquert le service jusqu'� la mort de l'objet
    };
	this.disconnect=function(){
		var callObj = buildCallObject("DisconnectClientApplication",0);
		App.adapter.callService(callbackDisconnectApplication,
                                callObj,
                                clientAppId);
		window.clearInterval(getEventIntervall);
		
	}
	function callbackDisconnectApplication(result) {
        if (result.error) {
            //console.error("CallMonitor: Disconnect(): " + result.errorDetail.string);
            return;
        }
        console.log("CallMonitor: Disconnect() returned successfully.");

      }
	 
		/**
		* Assign canTransfer variable
		**/
		

	function checkCanTransfer(){

		self.canTransfer = false;
		if(!lastVoiceCall) return;
		if(!lastVoiceCallState) return;
		if(!lastVoiceCall.Service) return;//private call
		//if(lastVoiceCallState.CurrentState != "Connected") return;//
		var stringVariables = lastVoiceCall.DisplayedVariables.Strings;
		var stringVariableValue = "";
		var length = stringVariables.length;
		var i = 0;
		for(i =0; i<length ; i++)
		{
			stringVariableValue = stringVariables[i].Value;
			stringVariableValue = stringVariableValue.replace(/^\s+|\s+$/g, '');
			if(stringVariableValue == blockTransferValue)
			{
				return;
			}
		}
		
		self.canTransfer = true;
	}
	
}
