/**
 * Adapter to the low-level component (WebService behavior in IE)
 * that provides SOAP capability.
 * @param url Address of web service definition (wsdl).
 *        example: "http://localhost/NASWebService/NASWebService.asmx?wsdl".
 * @param callback Function to invoke in case of successful initialization.
 * @constructor
 */
function WebServiceAdapter(url, callback) {
    /**
     * Address of web service definition (wsdl).
     * @private
     */
    this._url = url;

    /**
     * Whether the web methods can be invoked or not.
     * @private
     */
    this._serviceAvailable = false;

    /**
     * Number of seconds before aborting the service initialization.
     * @private
     */
    this._callTimeout = 0;

    /**
     * Success callback.
     */
    this._callback = callback;

    /* *****************
     * Constructor code
     */

    console.log("WebServiceAdapter created (" + url + ")");

    // Starts initialization.
    this._useService();
}

/**
 * Reference to instance that is currently initiating (used in event handlers).
 */
WebServiceAdapter.initiatingInstance = null;

/**
 * First initialization step.
 * @private
 */
function WebServiceAdapter_useService() {
    
    if (WebServiceAdapter.initiatingInstance !== null) {
        throw new Error("WebServiceAdapter_useService: only one instance can be initiated at a time.");
    }
	
    // Keep reference of current object (used in event handlers).
    WebServiceAdapter.initiatingInstance = this;
    
    // Create the call object with SOAPHeader array
    // Asynchronous communication with timeout of 0 sec
    callObj = createUseOptions();
    callObj.reuseConnection = false;
    callObj.timeout = this._callTimeout * 1000;
    callObj.SOAPHeader = new Array();
    callObj.SOAPHeader[0] = {};
        
    // once complete we can initialize the service
    if (useService(this._handleServiceAvailable,
                   this._url,
                   "NASWebService",
                   callObj)) {
        console.info("WSDL lookup at URL " + this._url);
    }
    else
    {
        this._serviceAvailable = false;
        WebServiceAdapter.initiatingInstance = null;
        console.error("Unable to initialize webservice adapter.");
    };
}

/**
 * Second (and final) initialization step.
 * @private
 */
function WebServiceAdapter_handleServiceAvailable(isServiceAvailable) {

    var self = WebServiceAdapter.initiatingInstance;

    if (isServiceAvailable) {
        self._serviceAvailable = true;
        WebServiceAdapter.initiatingInstance = null;
        
        console.info("WSDL lookup succeeded.");

        if (self._callback) {
            self._callback();
        }
    }
    else {
        self._serviceAvailable = false;
        WebServiceAdapter.initiatingInstance = null;
        console.error("WSDL lookup failed.");
    }
}


/**
 * Creates an instance of call object,
 * which can be passed as a parameter to the callService method.
 */
function WebServiceAdapter_createCallOptions() {
    return createCallOptions();
}


/**
 * Invokes a particular method of a web service.
 * The arguments of this method must be provided as additional arguments.
 * @param callBackFunction Function to be called when method is terminated.
 * @param fo One of the following possible values:
 *          - A string representing the name of the remote function being called.
 *            The string must be bounded by single or double quotation marks.
 *          - A call object, which has the necessary properties defined to call a remote function.
 */
function WebServiceAdapter_callService(callBackFunction,
                                       fo) {
    var i;
    var methodArguments;

    if (this._serviceAvailable) {

        if (fo.funcName === "DisconnectClientApplication") {
            // Do it now
            this.disableServiceAvailability(); 
        }

        // Prepare arguments.
        methodArguments = new Array(arguments.length);
        for (i = 0; i < arguments.length; i += 1) {
            methodArguments[i] = arguments[i];
        }
        
		// Call method with arguments of current method.
		callService(this._url, methodArguments);
		return true;
    }
    else {
        console.error("WebServiceAdapter_callService(): Service is not available.");
        return false;
    }
}


/**
 * Gets the next call id returned by the callService method.
 */
function WebServiceAdapter_getNextCallId() {
    return getNextId();
}

/**
 * Disable Service Availability.
 */
function WebServiceAdapter_disableServiceAvailability() {
    this._serviceAvailable = false;
}

/* ****************************************************************************
 * Private methods
 */

WebServiceAdapter.prototype._useService = WebServiceAdapter_useService;

WebServiceAdapter.prototype._handleServiceAvailable = WebServiceAdapter_handleServiceAvailable;


/* ****************************************************************************
 * Public methods
 */

WebServiceAdapter.prototype.createCallOptions = WebServiceAdapter_createCallOptions;

WebServiceAdapter.prototype.callService = WebServiceAdapter_callService;

WebServiceAdapter.prototype.getNextCallId = WebServiceAdapter_getNextCallId;

WebServiceAdapter.prototype.disableServiceAvailability = WebServiceAdapter_disableServiceAvailability;