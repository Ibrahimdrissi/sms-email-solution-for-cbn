var App = {
    /**
     * Adapter for invoking web service methods.
     */
    adapter: null,

    /**
     * Call monitor.
     */
    monitor: null,

    /**
     * Initialization method.
     */
    init: function() {
	 	this.monitor = new CallMonitor();
	 	this.adapter = new WebServiceAdapter("http://srvacp1/portal/webservice/naswebservice.asmx?wsdl",
                                             function() {App.monitor.init(); });
    }
	
	

};